# Paymob WHMCS

Paymob WHMCS module is integrating Paymob payment with WHMCS platform.

## Introduction

Paymob WHMCS module comes with unified checkout payment. It set up a wide range of local and international payment options to maximize revenue generation opportunities such as credit and debit cards, online mobile wallets, cash on delivery, loyalty points redem options, payment links and even kiosk payments through Aman and Masary.

## Installation Steps

    1. Extract the downloaded WHMCS module .zip file into your server in the path of WHMCS project.
    2. Login in to the WHMCS admin panel, navigate to Setup > Apps & integrations > Browse > Payments.
    3- Search for Paymob Payment, then click on Manage button.

## Admin Configurations

### In WHMCS Admin Panel, follow the steps below:

    1. Log into Paymob account > Setting in the left menu. Then get the Secret, public, API keys, HMAC and integration IDs. 
    2. In WHMCS admin, Navigate to Addons →  Apps & integrations →  Payments Apps.  . 
    3. Configure the new module in the Manage Existing Gateways tab.
    4. Add Paymob configuration as mentioned in point (1).
    5. Copy integration callback URL that exists in Paymob WHMCS setting page. Then, paste it into each payment integration/method in Paymob account.
    6. Then, click on save changes. 

